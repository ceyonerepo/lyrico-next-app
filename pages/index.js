import Navbar from '../src/Navbar';

function Index() {
    return (
        <div>
            <Navbar />
            <h1>This is the index page</h1>
        </div>
    );
}

export default Index;